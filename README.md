# Hangman
## This project is created in a Python crash course

![alt text](https://www.shutterstock.com/image-vector/hangman-game-260nw-623194223.jpg)

## Run

Hangman requires [python](https://www.python.org/) v3.0.0+ to run.

Run the folder "src" to play

```sh
./src
```