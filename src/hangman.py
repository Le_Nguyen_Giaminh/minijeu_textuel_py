import random

def play():
    word_list = ["lion", "tiger", "elephant", "giraffe", "zebra", "monkey", "kangaroo", "panda", "koala", "hippo"]
    word = random.choice(word_list).lower()
    guessed_letters = []
    attempts = 6

    print("This is an animal name\n")
    print("This word has " + str(len(word)) + " letters")

    while True:
        display_word = ""
        for letter in word:
            if letter in guessed_letters:
                display_word += letter + " "
            else:
                display_word += "_ "

        print("Guess the word:", display_word)

        if display_word.replace(" ", "") == word:
            print("Congratulations! You guessed the word correctly!")
            break

        if attempts == 0:
            print("\n------Game over! You ran out of attempts-----\n")
            print("The word was:", word)
            break

        guess = input("Guess a letter: ").lower()

        if guess in guessed_letters:
            print("You already guessed that letter. Try again!")
        elif len(guess) != 1 or not guess.isalpha():
            print("Invalid input. Please enter a single letter.")
        else:
            guessed_letters.append(guess)
            if guess not in word:
                attempts -= 1
                print("Wrong guess. Attempts remaining:", attempts)


